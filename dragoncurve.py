# Online Python compiler (interpreter) to run Python online.
# Write Python 3 code in this online editor and run it.
# Get started with interactive Python!
# Supports Python Modules: builtins, math,pandas, scipy 
# matplotlib.pyplot, numpy, operator, processing, pygal, random, 
# re, string, time, turtle, urllib.request
import matplotlib.pyplot as plt
from matplotlib import collections  as mc
from math import *


iter = { 
  'zero' : 1,
  'one' : 0.5,
  'two' : 0.25,
  'three' : 0.125,
  'four' : 0.0625,
  'five' : 0.03125,
  'six' : 0.015625,
  'seven' : 0.0078125,
  'eight' : 0.00390625,
  'nine' : 0.001953125,
}

S = [[0+0j, 1+0j]]

coords = []

def f1(z):
	return ((1+1j)*z)/2

def f2(z):
	return 1-((1-1j)*z)/2

def func(z1, z2, S, i):
	A = f1(z1)
	B = f1(z2)
	C = f2(z1)
	D = f2(z2)		
	line1 = [A, B]
	line2 = [C, D]
	length1 = sqrt(pow(B.imag-A.imag, 2) + pow(B.real-A.real, 2))
	length2 = sqrt(pow(D.imag-C.imag, 2) + pow(D.real-C.real, 2))
	
	if length1 > iter['eight']:
		func(A, B, S, i)
	else:
		S.append(line1)
		
	if length2 > iter['eight']:
		func(C, D, S, i)
	else:
		S.append(line2)
	
	

pair = S.pop()
func(pair[0], pair[1], S, 1)

  
for line in S:
  coords.append([(line[0].real, line[0].imag), (line[1].real, line[1].imag)])


lc = mc.LineCollection(coords)
fig, ax = plt.subplots()
ax.add_collection(lc)
ax.autoscale()
ax.margins(0.1)
plt.show()
  
  

#plt.plot(x, y)

#plt.show()
print("finished")
