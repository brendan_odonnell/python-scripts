
mat = [[1, -1, -2, 4],
       [2, -1, -1, 2],
       [2,  1,  4, 16]]

new = []
for row in mat:
    new.append([float(el) for el in row])


rows = len(new)

for i, row in enumerate(new):
    if row[0] != 0:
        place = rows - i
        if place == rows:
            for j in range(i+1, rows):
                factor = -1*(new[j][0]/row[0])
                new[j] = [a+(factor*b) for a,b in zip(new[j], row)]
            continue

    if row[1] != 0:
        place = rows - i
        if place != 0 and place != rows:
            for j in range(rows):
                if i == j:
                    continue
                factor = -1*(new[j][1]/row[1])
                new[j] = [a+(factor*b) for a,b in zip(new[j], row)]
            continue
    
    if row[2] != 0:
#        place = rows - i
#        if place == rows-1:
        for j in range(rows-1):
            factor = -1*(new[j][2]/row[2])
            new[j] = [a+(factor*b) for a,b in zip(new[j], row)]
        break;
print(new)


