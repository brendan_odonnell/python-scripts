# Online Python compiler (interpreter) to run Python online.
# Write Python 3 code in this online editor and run it.
# Get started with interactive Python!
# Supports Python Modules: builtins, math,pandas, scipy 
# matplotlib.pyplot, numpy, operator, processing, pygal, random, 
# re, string, time, turtle, urllib.request
import matplotlib.pyplot as plt


real = []
imag = []
N = 100
iter = 15

for i in range(-2*N, N+1):
  for j in range(-1*N, N+1):
    z = complex(i/N, j/N)
    temp = z
    for k in range(iter):
      z = (z * z) + temp
    if abs(z) < 2:
      real.append(i/N)
      imag.append(j/N)
  

plt.plot(real, imag, 'ro', markersize = .5)  
plt.show()
print("finished")
